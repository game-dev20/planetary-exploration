﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuilderGameObjectsFromMapArray : MonoBehaviour
{
    private CollectingItemsFromMap _prefabMapArea;

    public GameObject BuildingMap(CellType[,] levelMap)
    {
        _prefabMapArea = 
            Managers.GameParameters.DataLevelParameters.PrefabMapArea;

        GameObject gameObjectMap = new GameObject("map");
        CollectingItemsFromMap currentMapElements = null;

        int cardWidth = levelMap.GetUpperBound(0) + 1;
        int cardHeight = levelMap.GetUpperBound(1) + 1;

        CollectingItemsFromMap.ArrayMap = (CellType[,]) levelMap.Clone();
        CollectingItemsFromMap.CurrentArrayMap = (CellType[,]) levelMap.Clone();
        
        ConstructionOnMap.parentObject = gameObjectMap;

        for (int y = 0; y <= cardHeight; y++)
        {
            for (int x = -1; x <= cardWidth; x++)
            {
                if (x == -1 || x == cardWidth || y == cardHeight)
                {
                    Instantiate(_prefabMapArea, new Vector3(x, y, 0), 
                        Quaternion.identity, gameObjectMap.transform);
                    continue;
                }

                if(levelMap[x, y] == CellType.Air)
                {
                    continue;
                }

                currentMapElements = 
                    Instantiate(_prefabMapArea, new Vector3(x, y, 0), 
                    Quaternion.identity, gameObjectMap.transform);
                
                currentMapElements.CreateItemArea(x, y);
            }
        }

        return gameObjectMap;
    }

}
