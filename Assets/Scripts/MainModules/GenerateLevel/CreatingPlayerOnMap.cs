﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatingPlayerOnMap : MonoBehaviour
{
    private CellType[,] _levelMap;
    private GameObject _parentGameObject;
    private GameObject _playerPrefab;
    private Dictionary<string, System.Object> _mainParameters;
    
    public void InitialData(CellType[,] levelMap, GameObject parentGameObject)
    {
        _levelMap = levelMap;
        _parentGameObject = parentGameObject;

        _mainParameters = Managers.GameParameters.DictionaryMainParameters;

        _playerPrefab = (GameObject) _mainParameters["PlayerPrefab"];
    }

    public GameObject CreatePlayerOnMap()
    {
        GameObject createdPlayer = null;
        int height = _levelMap.GetUpperBound(1) + 1;
        int x = 2;
        int y = 0;

        for (y = 0; y < height - 1; y++)
        {
            if(
                (
                    (_levelMap[x, y] == CellType.Land) ||
                    (
                        (_levelMap[x, y] != CellType.Air) &&
                        (_levelMap[x, y] != CellType.Land)
                    )
                ) &&
                (_levelMap[x, y + 1] == CellType.Air)
            )
            {
                createdPlayer = Instantiate(_playerPrefab, 
                    new Vector3(x, y + 2f, 0.6f), Quaternion.identity, 
                    _parentGameObject.transform);

                Debug.Log("Player replaced on stage.");
                break;
            }
        }

        return createdPlayer;
    }
}
