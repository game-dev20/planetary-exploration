﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGameObjectsTreesFromMapArray : MonoBehaviour
{
    private CellType[,] _levelMap;
    private GameObject _parentGameObject;
    private List<GameObject[]> _treePrefab;
    private Dictionary<string, System.Object> _levelParameters;
    private int _idTypeTrees;

    public void InitialData(CellType[,] levelMap, GameObject parentGameObject)
    {
        _levelMap = levelMap;
        _parentGameObject = parentGameObject;

        _levelParameters = Managers.GameParameters.DictionaryLevelParameters;

        GameObject[] treeType1Prefab = 
            (GameObject[]) _levelParameters["TreeType1Prefab"];
        GameObject[] treeType2Prefab = 
            (GameObject[]) _levelParameters["TreeType2Prefab"];
        GameObject[] treeType3Prefab = 
            (GameObject[]) _levelParameters["TreeType3Prefab"];
        GameObject[] treeType4Prefab = 
            (GameObject[]) _levelParameters["TreeType4Prefab"];

        _treePrefab = new List<GameObject[]>();
        _treePrefab.Add(treeType1Prefab);
        _treePrefab.Add(treeType2Prefab);
        _treePrefab.Add(treeType3Prefab);
        _treePrefab.Add(treeType4Prefab);

        int seed = (int) _levelParameters["MapSeed"];
        UnityEngine.Random.InitState(seed);

        _idTypeTrees = UnityEngine.Random.Range(0, 4);
    }

    public void BuildingMap()
    {
        int width = _levelMap.GetUpperBound(0) + 1;
        int height = _levelMap.GetUpperBound(1) + 1;

        int distance = UnityEngine.Random.Range(4, 6);

        for (int x = 0; x < width; x++)
        {
            distance--;

            if (distance != 0)
            {
                continue;
            }

            for (int y = 0; y < height - 1; y++)
            {
                if (
                    (
                        (
                            (_levelMap[x, y] != CellType.Air) &&
                            (_levelMap[x, y] != CellType.Land)
                        ) ||
                        (_levelMap[x, y] == CellType.Land)
                    ) &&
                    (_levelMap[x, y + 1] == CellType.Air)
                )
                {
                    GameObject[] listTreePrefabs = _treePrefab[_idTypeTrees];
                    int countPrefabs = listTreePrefabs.Length;
                    int i = UnityEngine.Random.Range(0, countPrefabs);

                    GameObject treePrefab = listTreePrefabs[i];
                    
                    Instantiate(treePrefab, new Vector3(x , y, 0f), 
                        Quaternion.identity, _parentGameObject.transform);

                    distance = UnityEngine.Random.Range(4, 6);

                    break;
                }
            }
        }

        StandartInitialRandomaizer();
    }

    private void StandartInitialRandomaizer()
    {
        DateTime dateUnix = new DateTime(1970, 1, 1, 0, 0, 0);
        DateTime dateNow = DateTime.Now;
        int newSeed = (int)dateNow.Subtract(dateUnix).TotalSeconds;
        UnityEngine.Random.InitState(newSeed);
    }
}
