﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorPlanetMapArray : MonoBehaviour
{
    private int _sizeInWidth;
    private int _depthFromSurface;
    private int _heightFromSurface;

    public CellType[,] LevelMap { get; private set; }

    private Dictionary<string, System.Object> _levelParameters;

    public void Generate()
    {
        InitialData();

        Landscaping();

        GeneratingCaves();

        ResourceGeneration(
            (float) _levelParameters["ProbabilityAppearanceOriginalIronPoint"],
            (float) _levelParameters["IronProbabilityNeighboringCells"],
            CellType.Iron
        );

        ResourceGeneration(
            (float) _levelParameters["ProbabilityAppearanceOriginalUranusPoint"],
            (float) _levelParameters["UranusProbabilityNeighboringCells"],
            CellType.Uranus
        );

        StandartInitialRandomaizer();
    }

    private void StandartInitialRandomaizer()
    {
        DateTime dateUnix = new DateTime(1970, 1, 1, 0, 0, 0);
        DateTime dateNow = DateTime.Now;
        int newSeed = (int)dateNow.Subtract(dateUnix).TotalSeconds;
        UnityEngine.Random.InitState(newSeed);
    }

    private void InitialData()
    {
        _levelParameters = Managers.GameParameters.DictionaryLevelParameters;

        _sizeInWidth = (int)_levelParameters["LevelSizeInWidth"];

        _depthFromSurface = (int)_levelParameters["LevelDepthFromSurface"];

        _heightFromSurface = (int)_levelParameters["LevelHeightFromSurface"];

        int seed = (int)_levelParameters["MapSeed"];
        UnityEngine.Random.InitState(seed);

        LevelMap = null;

        LevelMap = new CellType[
            _sizeInWidth,
            2 * (_depthFromSurface + _heightFromSurface + 1)
        ];
        for (int x = 0; x < _sizeInWidth; x++)
        {
            for (int y = 0;
                y < 2 * (_depthFromSurface + _heightFromSurface + 1); y++)
            {
                LevelMap[x, y] = CellType.Air;
            }
        }
    }

    private void Landscaping()
    {
        int surfaceType = 0;
        int oldSurfaceType = -1;
        int amountFlatSurface = 15 + UnityEngine.Random.Range(0, 10);
        int currentAmountFlatSurface = 0;
        int currentSurfaceLevel = (_depthFromSurface + _heightFromSurface) / 2;
        int howHighChange = 0;

        for (int x = 0; x < _sizeInWidth; x++)
        {
            if (surfaceType == 0)
            {
                if (currentAmountFlatSurface == amountFlatSurface)
                {
                    surfaceType = UnityEngine.Random.Range(0, 2);
                    currentAmountFlatSurface = 0;
                    amountFlatSurface = 15 + UnityEngine.Random.Range(0, 10);
                    howHighChange = 1;

                    if (UnityEngine.Random.Range(0, 2) == 0)
                    {
                        currentSurfaceLevel += howHighChange;
                    }
                    else
                    {
                        currentSurfaceLevel -= howHighChange;
                    }
                }
            }
            else
            {
                howHighChange = UnityEngine.Random.Range(0, 5);

                if (currentAmountFlatSurface <= (amountFlatSurface / 2))
                {
                    currentSurfaceLevel += howHighChange;
                }
                else
                {
                    currentSurfaceLevel -= howHighChange;
                }

                if (currentAmountFlatSurface == amountFlatSurface)
                {
                    oldSurfaceType = surfaceType;
                    surfaceType = 0;
                    currentAmountFlatSurface = 0;
                    amountFlatSurface = 5 + UnityEngine.Random.Range(0, 10);
                }
            }

            currentSurfaceLevel = Mathf.Clamp(currentSurfaceLevel, 0,
                _depthFromSurface + _heightFromSurface);

            for (int y = currentSurfaceLevel; y >= 0; y--)
            {
                LevelMap[x, y] = CellType.Land;
            }

            currentAmountFlatSurface++;
        }
    }

    private void GeneratingCaves()
    {
        float likelihoodVoidsGround =
            (float)_levelParameters["LikelihoodVoidsGround"];
        int amountFlatSurface = 5 + UnityEngine.Random.Range(0, 10);
        int currentAmountFlatSurface = 0;
        int surfaceType = 0;

        for (int y = 1; y < _depthFromSurface + _heightFromSurface; y++)
        {
            for (int x = 0; x < _sizeInWidth; x++)
            {
                if ((surfaceType == 1) && (LevelMap[x, y] == CellType.Land))
                {
                    LevelMap[x, y] = CellType.EmptyLot;
                }

                currentAmountFlatSurface++;
                if (amountFlatSurface == currentAmountFlatSurface)
                {
                    amountFlatSurface = 5 + UnityEngine.Random.Range(0, 10);
                    currentAmountFlatSurface = 0;

                    if (UnityEngine.Random.Range(0f, 1f) <=
                        likelihoodVoidsGround)
                    {
                        surfaceType = 1;
                    }
                    else
                    {
                        surfaceType = 0;
                    }
                }
            }
        }
    }

    private void ResourceGeneration(
        float probabilityGrainAppearance,
        float probabilitySpread,
        CellType resourceType
    )
    {
        for (int y = 1; y < _depthFromSurface + _heightFromSurface; y++)
        {
            for (int x = 1; x < _sizeInWidth - 1; x++)
            {
                if (
                    (
                        UnityEngine.Random.Range(0f, 1f) <=
                        probabilityGrainAppearance
                    ) &&
                    (LevelMap[x, y] == CellType.Land)
                )
                {
                    LevelMap[x, y] = resourceType;
                }
            }
        }

        for (int i = 0; i < 10; i++)
        {
            for (int y = 1; y < _depthFromSurface + _heightFromSurface - 1; y++)
            {
                for (int x = 1; x < _sizeInWidth - 1; x++)
                {
                    if (
                        (
                            (LevelMap[x - 1, y - 1] == resourceType) ||
                            (LevelMap[x - 1, y] == resourceType) ||
                            (LevelMap[x - 1, y + 1] == resourceType) ||
                            (LevelMap[x, y + 1] == resourceType) ||
                            (LevelMap[x, y - 1] == resourceType) ||
                            (LevelMap[x + 1, y - 1] == resourceType) ||
                            (LevelMap[x + 1, y] == resourceType) ||
                            (LevelMap[x + 1, y + 1] == resourceType)
                        ) &&
                        (UnityEngine.Random.Range(0f, 1f) <= probabilitySpread)
                        &&
                        (LevelMap[x, y] == CellType.Land)
                    )
                    {
                        LevelMap[x, y] = resourceType;
                    }
                }
            }
        }
    }


}
