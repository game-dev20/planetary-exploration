﻿public static class GUIEvents
{
    public static string TURN_ON_OR_OFF_SATCHEL = "TURN_ON_OR_OFF_SATCHEL";
    public static string ON_OR_OFF_RESOURCE_EXTRACTION_MODE = 
        "ON_OR_OFF_RESOURCE_EXTRACTION_MODE";
    public static string OFF_RESOURCE_EXTRACTION_MODE = 
        "OFF_RESOURCE_EXTRACTION_MODE";
    public static string ON_OR_OFF_CONSTRUCTION_MODE = 
        "ON_OR_OFF_CONSTRUCTION_MODE";
    public static string OFF_CONSTRUCTION_MODE = "OFF_CONSTRUCTION_MODE";
}
