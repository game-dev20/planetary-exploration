﻿using UnityEngine;

[CreateAssetMenu(
    fileName = "LevelParameters", 
    menuName = "Create Level Parameters"
 )]
public class LevelParameters : ScriptableObject
{
    public int MapSeed;//сид на всю карту
    public int LevelSizeInWidth;//Размер уровня по ширине
    public int LevelDepthFromSurface;//глубина уровня от поверхности
    public int LevelHeightFromSurface;//высота уровня от поверхности
    public float ProbabilityAppearanceOriginalIronPoint;//вероятность появления первоначальной точки железа
    public float IronProbabilityNeighboringCells;//вероятность того что на соседних  клетках от клетки в которой есть железо тоже будет железо
    public float ProbabilityAppearanceOriginalUranusPoint;//вероятность появления первоначальной точки урана
    public float UranusProbabilityNeighboringCells;//вероятность того что на соседних клетках от клетки в которой есть уран тоже будет уран
    public float LikelihoodVoidsGround;//вероятность появления пустот в земле
    
    //префабы на части карты
    public GameObject PrefabAirEmpty;
    public GameObject PrefabAirEmptyAir;
    public GameObject PrefabAirLandAir;
    public GameObject PrefabAirLandEmpty;
    public GameObject PrefabAirLandLand;
    public GameObject PrefabEmpty;
    public GameObject PrefabEmptyAir;
    public GameObject PrefabEmptyLandAir;
    public GameObject PrefabEmptyLandEmpty;
    public GameObject PrefabEmptyLandLand;
    public GameObject PrefabLand;
    public GameObject PrefabLandLandAir;
    public GameObject PrefabLandLandEmpty;
    public GameObject PrefabResurce;
    public GameObject PrefabToHighlightAddition;
    //-----------------

    //префабы на части строительных блоков
    public GameObject PrefabDoorUnit;
    public GameObject PrefabWallBlock;
    public GameObject PrefabCanAddedDoorUnit;
    public GameObject PrefabCanAddedWallBlock;
    //-----------------

    public ConstructionOnMap PrefabBuildingBlock;
    public CollectingItemsFromMap PrefabMapArea;
    public Color ColorIron;//цвет ресурса железа
    public Color ColorUranus;//цвет ресурса уран
    public GameObject[] TreeType1Prefab;//префабы деревьев тип 1 
    public GameObject[] TreeType2Prefab;//префабы деревьев тип 2 
    public GameObject[] TreeType3Prefab;//префабы деревьев тип 3 
    public GameObject[] TreeType4Prefab;//префабы деревьев тип 4 
}
