﻿using UnityEngine;

[CreateAssetMenu(
    fileName = "MainParameters",
    menuName = "Create Main Parameters"
 )]
public class MainParameters : ScriptableObject
{
    public string PlanetSceneName;//название сцены планеты
    public GameObject PlayerPrefab;//префаб игрока
    public Color ColorSelectedCellDelete;//цвет выделенной ячейки на удаление
}
