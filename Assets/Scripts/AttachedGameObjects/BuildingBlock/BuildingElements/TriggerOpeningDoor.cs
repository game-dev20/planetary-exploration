﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOpeningDoor : MonoBehaviour
{
    [SerializeField] private GameObject _objectDoor;

    private Vector3 _startPos;

    public Vector3 openPos = new Vector3(0f, 0f, 0.95f);

    private void Awake()
    {
        _startPos = _objectDoor.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        iTween.MoveTo(_objectDoor, (_startPos + openPos), 0.5f);
    }

    private void OnTriggerExit(Collider collider)
    {
        iTween.MoveTo(_objectDoor, _startPos, 0.5f);
    }
}
