﻿using System;
using System.Collections;
using UnityEngine;

public class TriggerElevatorActivate : MonoBehaviour
{
    public bool isTriggerActivate = false;
    public Action callback = null;
    
    private void OnTriggerEnter(Collider other)
    {
        isTriggerActivate = true;
        callback();
    }

    private void OnTriggerExit(Collider other)
    {
        isTriggerActivate = false;
        callback();
    }
}
