﻿using System.Collections;
using UnityEngine;

public class OperationLift : MonoBehaviour
{
    [SerializeField] private TriggerElevatorActivate _triggerActivateLift;
    [SerializeField] private TriggerElevatorActivate _triggerDown;
    [SerializeField] private TriggerElevatorActivate _triggerUp;
    [SerializeField] private GameObject _area;
    [SerializeField] private GameObject _piston1;
    [SerializeField] private GameObject _piston2;
    [SerializeField] private Vector3 _finichPosArea;
    [SerializeField] private Vector3 _finichPosPiston1;
    [SerializeField] private Vector3 _finichPosPiston2;
    [SerializeField] private float _speedLift = 1.0f;
    [SerializeField] private float _waitingBeforeStartingLift = 1.0f;
    [SerializeField] private ElevatorCondition _elevatorCondition = 
        ElevatorCondition.Down;
    
    private bool _isWorkingState = false;
    private ElevatorCondition _currentDirectionTravel = ElevatorCondition.Down;
    private Vector3 _startPosArea;
    private Vector3 _startPosPiston1;
    private Vector3 _startPosPiston2;
    private Transform _currentTransformArea;
    private Transform _currentTransformPiston1;
    private Transform _currentTransformPiston2;

    private enum ElevatorCondition
    {
        Down,
        Up
    }

    private void Awake()
    {
        _triggerActivateLift.callback = Operate;
        _triggerDown.callback = Operate;
        _triggerUp.callback = Operate;

        _startPosArea = _area.transform.position;
        _startPosPiston1 = _piston1.transform.position;
        _startPosPiston2 = _piston2.transform.position;

        _currentTransformArea = _area.transform;
        _currentTransformPiston1 = _piston1.transform;
        _currentTransformPiston2 = _piston2.transform;
}

    public void Operate()
    {
        if(_isWorkingState)
        {
            return;
        }

        if (_triggerActivateLift.isTriggerActivate)
        {
            StartCoroutine(ExpectBeforeStart());
        }
        else if (
            _triggerDown.isTriggerActivate && 
            (_elevatorCondition == ElevatorCondition.Up)
        )
        {
            StartLoweringElevator();
        }
        else if (
            _triggerUp.isTriggerActivate &&
            (_elevatorCondition == ElevatorCondition.Down)
        )
        {
            StartLiftingElevator();
        }
    }

    private void Update()
    {
        if (_isWorkingState)
        {
            Vector3 moving = Vector3.zero;
            Vector3 finishPosArea = Vector3.zero;
            Vector3 finishPosPiston1 = Vector3.zero;
            Vector3 finishPosPiston2 = Vector3.zero;

            if (_currentDirectionTravel == ElevatorCondition.Down)
            {
                moving = -_finichPosArea * _speedLift;

                finishPosArea = _startPosArea;
                finishPosPiston1 = _startPosPiston1;
                finishPosPiston2 = _startPosPiston2;
            }
            else
            {
                moving = _finichPosArea * _speedLift;

                finishPosArea = _startPosArea + _finichPosArea;
                finishPosPiston1 = _startPosPiston1 + _finichPosPiston1;
                finishPosPiston2 = _startPosPiston2 + _finichPosPiston2;
            }

            moving *= Time.deltaTime;

            _currentTransformArea.position += moving;
            _currentTransformPiston1.position += moving;
            _currentTransformPiston2.position += moving;

            if (
                Vector3.Distance(_currentTransformPiston2.position, 
                finishPosPiston2) <= 0.1f
            )
            {
                _currentTransformPiston2.position = finishPosPiston2;
            }
            if (
                Vector3.Distance(_currentTransformPiston1.position, 
                finishPosPiston1) <= 0.1f
            )
            {
                _currentTransformPiston1.position = finishPosPiston1;
            }
            if (
                Vector3.Distance(_currentTransformArea.position, 
                finishPosArea) <= 0.1f
            )
            {
                _currentTransformArea.position = finishPosArea;

                _isWorkingState = false;
                ChangeElevatorCondition();
            }
        }
    }

    private void StartLiftingElevator()
    {
        _isWorkingState = true;
        _currentDirectionTravel = ElevatorCondition.Up;
    }

    private void StartLoweringElevator()
    {
        _isWorkingState = true;
        _currentDirectionTravel = ElevatorCondition.Down;
    }


    private IEnumerator ExpectBeforeStart()
    {

        yield return new WaitForSeconds(_waitingBeforeStartingLift);

        if(
            _triggerActivateLift.isTriggerActivate &&
            (_elevatorCondition == ElevatorCondition.Up)
        )
        {
            StartLoweringElevator();
        }
        else if(
            _triggerActivateLift.isTriggerActivate &&
            (_elevatorCondition == ElevatorCondition.Down)
        )
        {
            StartLiftingElevator();
        }
    }

    private void ChangeElevatorCondition()
    {
        if(_elevatorCondition == ElevatorCondition.Down)
        {
            _elevatorCondition = ElevatorCondition.Up;
        }
        else
        {
            _elevatorCondition = ElevatorCondition.Down;
        }
    }
}