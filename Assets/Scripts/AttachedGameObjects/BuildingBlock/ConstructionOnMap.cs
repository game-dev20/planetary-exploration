﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructionOnMap : MonoBehaviour
{
    private static ConstructionOnMap _prefabBuildingBlock;
    private static GameObject _prefabDoorUnit;
    private static GameObject _prefabWallBlock;
    private static GameObject _prefabCanAddedDoorUnit;
    private static GameObject _prefabCanAddedWallBlock;

    private static GameObject _elementToHighlightAddition = null;
    private static Dictionary<string, System.Object> _levelParameters = null;
    private static int _targetX = 0;
    private static int _targetY = 0;
    private static bool _isPossibleAddCell = false;
    private static EnumModeConstruction _targetCurrentMode = 
        EnumModeConstruction.ModeRemove;

    private GameObject _indoorUnit = null;

    public static GameObject parentObject = null;
    public static int cardWidth = 0;
    public static int cardHeight = 0;
    public static TypesConstructionObjects[,] 
        ArrayConstructionObjectsOnMap = null;
    public static ConstructionOnMap[,] identity = null;

    public enum EnumModeConstruction
    {
        ModeRemove,
        ModeAddDoor,
        ModeAddWall
    }
    
    public enum TypesConstructionObjects
    {
        Empty,
        Door,
        Wall
    }

    void Awake()
    {
        
    }

    private static void InitialData()
    {
        if (_levelParameters == null)
        {
            _levelParameters =
                Managers.GameParameters.DictionaryLevelParameters;

            _prefabBuildingBlock =
                (ConstructionOnMap)_levelParameters["PrefabBuildingBlock"];
            _prefabDoorUnit =
                (GameObject)_levelParameters["PrefabDoorUnit"];
            _prefabWallBlock =
                (GameObject)_levelParameters["PrefabWallBlock"];
            _prefabCanAddedDoorUnit =
                (GameObject)_levelParameters["PrefabCanAddedDoorUnit"];
            _prefabCanAddedWallBlock =
                (GameObject)_levelParameters["PrefabCanAddedWallBlock"];
        }

        if (identity == null)
        {
            cardWidth = 
                CollectingItemsFromMap.CurrentArrayMap.GetUpperBound(0) + 1;
            cardHeight = 
                CollectingItemsFromMap.CurrentArrayMap.GetUpperBound(1) + 1;
            identity = new ConstructionOnMap[cardWidth, cardHeight];
            ArrayConstructionObjectsOnMap =
                new TypesConstructionObjects[cardWidth, cardHeight];
        }
    }

    public static void SetTargetXY(int x, int y, 
        EnumModeConstruction currentMode)
    {
        InitialData();

        if (
            (x > cardWidth - 1) ||
            (x < 0) ||
            (y > cardHeight - 1) ||
            (y < 0)
        )
        {
            return;
        }

        if (
            (x != _targetX) ||
            (y != _targetY) ||
            (_targetCurrentMode != currentMode)
        )
        {
            if (_targetCurrentMode != currentMode)
            {
                DestriyElementToHighlightAddition();
            }

            if (identity[_targetX, _targetY] != null)
            {
                identity[_targetX, _targetY].DeselectCellToRemove();
            }

            _targetCurrentMode = currentMode;

            _targetX = x;
            _targetY = y;
            
            if(_targetCurrentMode == EnumModeConstruction.ModeRemove)
            {
                if(identity[_targetX, _targetY] != null)
                {
                    identity[_targetX, _targetY].SelectCellToRemove();
                }
            }
            else
            {
                SelectCellToAdd();
            }
        }
    }

    private void SelectCellToRemove()
    {
        if (_indoorUnit != null)
        {
            _indoorUnit.SendMessage("SelectCellToRemove", 
                SendMessageOptions.DontRequireReceiver);
        }
    }

    private void DeselectCellToRemove()
    {
        if (_indoorUnit != null)
        {
            _indoorUnit.SendMessage("DeselectCellToRemove", 
                SendMessageOptions.DontRequireReceiver);
        }
    }

    private static void SelectCellToAdd()
    {
        if (
            (_targetX == 0) ||
            (_targetX == cardWidth - 1) ||
            (_targetY == 0) ||
            (_targetY == cardHeight - 1)
        )
        {
            DestriyElementToHighlightAddition();
            return;
        }

        if (
            (
                (
                    CollectingItemsFromMap
                    .CurrentArrayMap[_targetX, _targetY] != CellType.EmptyLot
                ) &&
                (
                    CollectingItemsFromMap
                    .CurrentArrayMap[_targetX, _targetY] != CellType.Air
                )
            ) ||
            (
                ArrayConstructionObjectsOnMap[_targetX, _targetY] != 
                TypesConstructionObjects.Empty
            )
        )
        {
            DestriyElementToHighlightAddition();
            return;
        }

        if (
            (
                CollectingItemsFromMap
                .CurrentArrayMap[_targetX - 1, _targetY] == CellType.Air
            ) &&
            (
                CollectingItemsFromMap
                .CurrentArrayMap[_targetX, _targetY + 1] == CellType.Air
            ) &&
            (
                CollectingItemsFromMap
                .CurrentArrayMap[_targetX + 1, _targetY] == CellType.Air
            ) &&
            (
                CollectingItemsFromMap
                .CurrentArrayMap[_targetX, _targetY - 1] == CellType.Air
            ) &&
            (
                ArrayConstructionObjectsOnMap[_targetX - 1, _targetY] == 
                TypesConstructionObjects.Empty
            ) &&
            (
                ArrayConstructionObjectsOnMap[_targetX, _targetY + 1] == 
                TypesConstructionObjects.Empty
            ) &&
            (
                ArrayConstructionObjectsOnMap[_targetX + 1, _targetY] == 
                TypesConstructionObjects.Empty
            ) &&
            (
                ArrayConstructionObjectsOnMap[_targetX, _targetY - 1] == 
                TypesConstructionObjects.Empty
            )
        )
        {
            DestriyElementToHighlightAddition();
            return;
        }

        CreateElementToHighlightAddition();

        _elementToHighlightAddition.transform.position =
            new Vector3(_targetX, _targetY, 0);
    }

    private static void CreateElementToHighlightAddition()
    {
        _isPossibleAddCell = true;

        if (_elementToHighlightAddition != null)
        {
            return;
        }

        GameObject prefabElement = null;
        
        if (_targetCurrentMode == EnumModeConstruction.ModeAddDoor)
        {
            prefabElement = _prefabCanAddedDoorUnit;
        }
        else if(_targetCurrentMode == EnumModeConstruction.ModeAddWall)
        {
            prefabElement = _prefabCanAddedWallBlock;
        }
        
        _elementToHighlightAddition = Instantiate(prefabElement, Vector3.zero, 
            Quaternion.identity, parentObject.transform);
    }

    private static void DestriyElementToHighlightAddition()
    {
        _isPossibleAddCell = false;

        if (_elementToHighlightAddition == null)
        {
            return;
        }

        Destroy(_elementToHighlightAddition);

        _elementToHighlightAddition = null;
    }

    public static void Operate()
    {
        if (
            (identity == null) ||
            (_targetX > cardWidth - 1) ||
            (_targetX < 0) ||
            (_targetY > cardHeight - 1) ||
            (_targetY <= 0)
        )
        {
            return;
        }

        if (_targetCurrentMode == EnumModeConstruction.ModeRemove)
        {
            if(
                ArrayConstructionObjectsOnMap[_targetX, _targetY] != 
                TypesConstructionObjects.Empty
            )
            {
                switch(ArrayConstructionObjectsOnMap[_targetX, _targetY])
                {
                    case TypesConstructionObjects.Door:
                        Managers.Inventory
                            .AddItem(ItemsMayInventory.RecycledIron, 25);
                        break;
                    case TypesConstructionObjects.Wall:
                        Managers.Inventory
                            .AddItem(ItemsMayInventory.RecycledIron, 50);
                        break;
                }

                ArrayConstructionObjectsOnMap[_targetX, _targetY] =
                    TypesConstructionObjects.Empty;

                if (identity[_targetX, _targetY] != null)
                {
                    Destroy(identity[_targetX, _targetY].gameObject);
                }
            }
        }
        else
        {
            TypesConstructionObjects typeObjectInsert;
            GameObject prefabElement = null;

            if (_targetCurrentMode == EnumModeConstruction.ModeAddDoor)
            {
                typeObjectInsert = TypesConstructionObjects.Door;
                prefabElement = _prefabDoorUnit;
            }
            else
            {
                typeObjectInsert = TypesConstructionObjects.Wall;
                prefabElement = _prefabWallBlock;
            }

            if (
                _isPossibleAddCell && 
                (
                    ArrayConstructionObjectsOnMap[_targetX, _targetY] == 
                    TypesConstructionObjects.Empty
                )
            )
            {
                StatusesUsingItemsInventory statusesUsing = 
                    StatusesUsingItemsInventory.NotInInventory;

                switch (typeObjectInsert)
                {
                    case TypesConstructionObjects.Door:
                        statusesUsing = Managers.Inventory
                            .ConsumeItem(ItemsMayInventory.RecycledIron, 50);
                        break;
                    case TypesConstructionObjects.Wall:
                        statusesUsing = Managers.Inventory
                            .ConsumeItem(ItemsMayInventory.RecycledIron, 100);
                        break;
                }

                if (statusesUsing == StatusesUsingItemsInventory.Used)
                {
                    ArrayConstructionObjectsOnMap[_targetX, _targetY] = 
                        typeObjectInsert;

                    identity[_targetX, _targetY] = Instantiate(
                        _prefabBuildingBlock, 
                        new Vector3(_targetX, _targetY, 0), Quaternion.identity, 
                        parentObject.transform);

                    Transform transformCreatedObject = 
                        identity[_targetX, _targetY].gameObject.transform;

                    identity[_targetX, _targetY]._indoorUnit = Instantiate(
                        prefabElement, new Vector3(_targetX, _targetY, 0), 
                        Quaternion.identity, transformCreatedObject);
                }
            }
        }
    }
}