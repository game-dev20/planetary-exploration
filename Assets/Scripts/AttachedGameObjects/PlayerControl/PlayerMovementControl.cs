﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovementControl : MonoBehaviour
{
    public float minFall = -1.5f;
    public float moveSpeed = 6.0f;
    public float jumpSpeed = 15.0f;
    public float gravity = -9.8f;
    public float terminalVelocity = -10.0f;
    public float moveFlyingForce = 3.0f;
    public float maxFlySpeed = 10.0f;
    public float freezingFlyingSpeed = 1.0f;
    public bool isSatchelIncluded { get; private set; } = false;
    public int fuelConsumptionRate = 1;
    public string fuelResourceName = "FuelCell";

    [SerializeField] private ParticleSystem _turbineFireEffects;

    private int _fuelQuantity;
    private float _spentFuel;
    private CharacterController _charController;
    private CapsuleCollider _collider;
    private float _vertSpeed;
    private float _horSpeed;
    private ControllerColliderHit _contact;
    private Animator _animator;

    private bool _isHitPlatform = false;

    void Awake()
    {
        TurbineEffectControl();
        Messenger<bool>.AddListener(GUIEvents.TURN_ON_OR_OFF_SATCHEL, 
            TurnSatchelOnOrOff);
    }

    void Destroy()
    {
        Messenger<bool>.RemoveListener(GUIEvents.TURN_ON_OR_OFF_SATCHEL,
            TurnSatchelOnOrOff);
    }

    void TurbineEffectControl()
    {
        if (_turbineFireEffects != null)
        {
            if (isSatchelIncluded)
            {
                _turbineFireEffects.Stop(true, 
                    ParticleSystemStopBehavior.StopEmittingAndClear);
                _turbineFireEffects.Play(true);
            }
            else
            {
                _turbineFireEffects.Stop(true,
                    ParticleSystemStopBehavior.StopEmitting);
            }
        }
    }

    void TurnSatchelOnOrOff(bool value)
    {
        isSatchelIncluded = value;
        _animator.SetBool("Jumping", isSatchelIncluded);
        _fuelQuantity = Managers.Inventory.GetItemCount(fuelResourceName);
        _spentFuel = 0f;
        TurbineEffectControl();
        Debug.Log("Activation of the backpack, its state: " + value);
    }

    void Start()
    {
        _charController = GetComponent<CharacterController>();
        _collider = GetComponent<CapsuleCollider>();
        _vertSpeed = minFall;
        _horSpeed = 0f;
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if(!isSatchelIncluded)
        {
            Walking();
        }
        else
        {
            MovingFlight();
        }
    }

    bool FuelCombustion()
    {
        if (_fuelQuantity == 0)
        {
            TurnSatchelOnOrOff(false);
            Messenger.Broadcast(InventoryEvents.FUEL_BACKPACK_OUT);
            return false;
        }

        _spentFuel += fuelConsumptionRate * Time.deltaTime;
        if (_spentFuel >= 1.0f)
        {
            StatusesUsingItemsInventory statusUsingFuel =
                Managers.Inventory.ConsumeItem(fuelResourceName,
                (int)_spentFuel);

            _spentFuel = 0f;

            if (statusUsingFuel != StatusesUsingItemsInventory.Used)
            {
                TurnSatchelOnOrOff(false);
                Messenger.Broadcast(InventoryEvents.FUEL_BACKPACK_OUT);
                return false;
            }
        }

        return true;
    }

    void MovingFlight()
    {
        if(!FuelCombustion())
        {
            return;
        }

        Vector3 movemenFlyingt = Vector3.zero;

        float horInput = Input.GetAxis("Horizontal");
        float vertInput = Input.GetAxis("Vertical");

        if(horInput != 0 || vertInput != 0)
        {
            movemenFlyingt.x = horInput * moveFlyingForce;
            movemenFlyingt.y = vertInput * moveFlyingForce;

            movemenFlyingt = Vector3.ClampMagnitude(movemenFlyingt, 
                moveFlyingForce);
        }

        if(movemenFlyingt.sqrMagnitude > 0)
        {
            _vertSpeed += movemenFlyingt.y;
            _horSpeed += movemenFlyingt.x;
        }
        else
        {
            _vertSpeed = Mathf.Lerp(_vertSpeed, -freezingFlyingSpeed / 2, 
                freezingFlyingSpeed * Time.deltaTime);
            _horSpeed = Mathf.Lerp(_horSpeed, 0f, 
                freezingFlyingSpeed * Time.deltaTime);
        }

        Vector3 accumulatedSpeed = new Vector3(_horSpeed, _vertSpeed, 0f);
        accumulatedSpeed = Vector3.ClampMagnitude(accumulatedSpeed,
            maxFlySpeed);

        _horSpeed = accumulatedSpeed.x;
        _vertSpeed = accumulatedSpeed.y;
        
        movemenFlyingt.x = _horSpeed;
        movemenFlyingt.y = _vertSpeed;

        movemenFlyingt *= Time.deltaTime;

        _isHitPlatform = false;
        _charController.enabled = true;
        _collider.enabled = false;
        transform.parent = null;
        _charController.Move(movemenFlyingt);
    }

    void Walking()
    {
        Vector3 movement = Vector3.zero;

        float horInput = Input.GetAxis("Horizontal");

        if (horInput != 0)
        {
            movement.x = horInput * moveSpeed;
        }


        bool hitGround = false;
        RaycastHit hit;
        Platform platform = null;
        if (_vertSpeed < 0 &&
           Physics.Raycast(_charController.bounds.center, Vector3.down, out hit))
        {
            float scale = transform.localScale.y;
            float check = scale * 
                (_charController.height / 1.9f + _charController.radius);
            hitGround = hit.distance <= check;
            
            if(hitGround)
            {
                platform = hit.transform.GetComponent<Platform>();
            }
        }

        float coincidenceWithDirection = Vector3.Dot(
            new Vector3(movement.x, 0f, 0f), transform.forward);
        float directionalSpeed = Mathf.Abs(movement.x) * 
            coincidenceWithDirection;

        _animator.SetFloat("Speed", directionalSpeed);        

        if (hitGround)
        {
            if (Input.GetButtonDown("Jump"))
            {
                _vertSpeed = jumpSpeed;
            }
            else
            {
                _vertSpeed = minFall;
                _animator.SetBool("Jumping", false);
            }
        }
        else
        {
            _vertSpeed += gravity * 5 * Time.deltaTime;
            if (_vertSpeed < terminalVelocity)
            {
                _vertSpeed = terminalVelocity;
            }

            if (_contact != null)
            {
                _animator.SetBool("Jumping", true);
            }

            if (_charController.isGrounded)
            {
                if (Vector3.Dot(movement, _contact.normal) < 0)
                {
                    movement = _contact.normal * moveSpeed;
                }
                else
                {
                    movement += _contact.normal * moveSpeed;
                }
            }
        }
        movement.y = _vertSpeed;

        movement *= Time.deltaTime;

        if(platform != null)
        {
            _charController.enabled = false;
            _collider.enabled = true;
            transform.parent = platform.transform;
            movement.y = 0f;

            if (!_isHitPlatform)
            {
                _isHitPlatform = true;
                Vector3 positionPlatform = platform.transform.position;
                float heightPlatform = platform.transform.localScale.y;
                float positionY = positionPlatform.y + heightPlatform / 1.9f;
                Vector3 currentPosition = transform.position;
                transform.position = new Vector3(currentPosition.x, positionY,
                    currentPosition.z);
            }

            transform.Translate(movement, Space.World);
        }
        else
        {
            _isHitPlatform = false;
            _charController.enabled = true;
            _collider.enabled = false;
            transform.parent = null;
            _charController.Move(movement);
        }
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        _contact = hit;

        if(isSatchelIncluded)
        {
            _horSpeed = -_horSpeed / 10;
            _vertSpeed = -_vertSpeed / 10;
        }
    }
}
