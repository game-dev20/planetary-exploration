﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

class PlayerAbilityInteractPartsMap : MonoBehaviour
{
    [SerializeField] private Transform _headPlayer;

    private Vector3 _directionGaze = Vector3.zero;
    private Vector3 _positionHead = Vector3.zero;
    private int targetX = 0;
    private int targetY = 0;

    public Vector3 offset = Vector3.zero;
    public float radius = 1.0f;
    public bool isResourceExtractionMode { get; private set; } = false;
    public CollectingItemsFromMap.EnumResourceExtractionMode currentMode =
        CollectingItemsFromMap.EnumResourceExtractionMode.modeRemoveCell;

    public bool isConstructionMode { get; private set; } = false;
    public ConstructionOnMap.EnumModeConstruction currentConstructionMode =
        ConstructionOnMap.EnumModeConstruction.ModeRemove;

    void Awake()
    {
        Messenger<bool, CollectingItemsFromMap.EnumResourceExtractionMode>
            .AddListener(GUIEvents.ON_OR_OFF_RESOURCE_EXTRACTION_MODE, 
            OnOffResourceExtractionMode);
        Messenger<bool, ConstructionOnMap.EnumModeConstruction>
            .AddListener(GUIEvents.ON_OR_OFF_CONSTRUCTION_MODE, 
            OnOffConstructionMode);
    }

    void Destroy()
    {
        Messenger<bool, CollectingItemsFromMap.EnumResourceExtractionMode>
            .RemoveListener(GUIEvents.ON_OR_OFF_RESOURCE_EXTRACTION_MODE, 
            OnOffResourceExtractionMode);
        Messenger<bool, ConstructionOnMap.EnumModeConstruction>
            .RemoveListener(GUIEvents.ON_OR_OFF_CONSTRUCTION_MODE, 
            OnOffConstructionMode);
    }

    void OnOffResourceExtractionMode(bool value, 
        CollectingItemsFromMap.EnumResourceExtractionMode valueMode)
    {
        isResourceExtractionMode = value;
        currentMode = valueMode;

        if(!isResourceExtractionMode)
        {
            targetX = CollectingItemsFromMap.cardWidth - 1;
            targetY = CollectingItemsFromMap.cardHeight - 1;

            CollectingItemsFromMap.SetTargetXY(targetX, targetY, currentMode);
        }

        Debug.Log("Activating the resource collection mode, its state: " + 
            value + " " + valueMode);
    }

    void OnOffConstructionMode(bool value, 
        ConstructionOnMap.EnumModeConstruction valueMode)
    {
        isConstructionMode = value;
        currentConstructionMode = valueMode;

        if (!isConstructionMode)
        {
            targetX = ConstructionOnMap.cardWidth - 1;
            targetY = ConstructionOnMap.cardHeight - 1;

            ConstructionOnMap.SetTargetXY(targetX, targetY, currentConstructionMode);
        }

        Debug.Log("Activation of the construction mode, its status: " + value + 
            " " + valueMode);
    }

    void Update()
    {
        if (!isResourceExtractionMode && !isConstructionMode)
        {
            return;
        }

        _positionHead = _headPlayer.position + offset;
        _directionGaze = -1 * _headPlayer.up;

        targetX = Mathf.RoundToInt(_positionHead.x + _directionGaze.x * radius);
        targetY = Mathf.RoundToInt(_positionHead.y + _directionGaze.y * radius);

        if(isResourceExtractionMode)
        {
            CollectingItemsFromMap.SetTargetXY(targetX, targetY, currentMode);
        }
        else
        {
            ConstructionOnMap
                .SetTargetXY(targetX, targetY, currentConstructionMode);
        }

        if (
            Input.GetMouseButtonDown(0) &&
            !EventSystem.current.IsPointerOverGameObject()
        )
        {
            if(isResourceExtractionMode)
            {
                CollectingItemsFromMap.Operate();
            }
            else
            {
                ConstructionOnMap.Operate();
            }
        }
    }
}
