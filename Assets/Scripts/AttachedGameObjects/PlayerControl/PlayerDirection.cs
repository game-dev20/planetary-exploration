﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerDirection : MonoBehaviour
{
    public float rotSpeed = 15.0f;
    public Vector3 startDirection = new Vector3(0f, 90f, 0f);
    public float offsetZ = 0f;

    private Animator _animator;
    private Vector3 _pointWhereLook;
    private Quaternion _direction;
    private Camera _camera = null;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _pointWhereLook = Vector3.zero;
        _direction.eulerAngles = startDirection;
    }

    void LateUpdate()
    {
        if(_camera == null)
        {
            _camera = Camera.main;
        }

        Vector3 screenPosition = Input.mousePosition;
        screenPosition.z = transform.position.z + offsetZ;

        _pointWhereLook = Camera.main.ScreenToWorldPoint(screenPosition);

        if (_pointWhereLook.x >= transform.position.x)
        {
            _direction.eulerAngles = new Vector3(0f, 90f, 0f);
        }
        else
        {
            _direction.eulerAngles = new Vector3(0f, -90f, 0f);
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, _direction, 
            rotSpeed * Time.deltaTime);
    }

    void OnAnimatorIK()
    {
        if(_pointWhereLook != Vector3.zero)
        {
            _animator.SetLookAtWeight(1f, 1f, 1f, 0f, 1f);
            _animator.SetLookAtPosition(_pointWhereLook);
        }
    }
}
