﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionToPlayer : MonoBehaviour
{
    private Transform _target = null;
    private float _rotY;
    private Vector3 _offset;

    void Awake()
    {
        Messenger<Transform>.AddListener(LoadEvents.PLANET_LEVEL_BUILT,
            SetTargetObject
        );
    }

    void Destroy()
    {
        Messenger<Transform>.RemoveListener(LoadEvents.PLANET_LEVEL_BUILT,
            SetTargetObject);
    }
    public void SetTargetObject(Transform targetObject)
    {
        _target = targetObject;
        _rotY = transform.eulerAngles.y;
        transform.position = new Vector3(_target.position.x, _target.position.y, 
            transform.position.z);
        _offset = _target.position - transform.position;
    }

    void LateUpdate()
    {
        if(_target == null)
        {
            return;
        }

        Quaternion rotation = Quaternion.Euler(0, _rotY, 0);
        transform.position = _target.position - (rotation * _offset);
        transform.LookAt(_target);
    }
}
