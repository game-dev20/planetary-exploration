﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectingItemsFromMap : MonoBehaviour
{
    private static CollectingItemsFromMap _prefabMapArea;
    private static GameObject _prefabAirEmpty;
    private static GameObject _prefabAirEmptyAir;
    private static GameObject _prefabAirLandAir;
    private static GameObject _prefabAirLandEmpty;
    private static GameObject _prefabAirLandLand;
    private static GameObject _prefabEmpty;
    private static GameObject _prefabEmptyAir;
    private static GameObject _prefabEmptyLandAir;
    private static GameObject _prefabEmptyLandEmpty;
    private static GameObject _prefabEmptyLandLand;
    private static GameObject _prefabLand;
    private static GameObject _prefabLandLandAir;
    private static GameObject _prefabLandLandEmpty;
    private static GameObject _prefabResurce;
    private static GameObject _prefabToHighlightAddition;

    private static GameObject _elementToHighlightAddition = null;
    private static GameObject _parentObject = null;
    private static Dictionary<string, System.Object> _levelParameters = null;
    private static int _targetX = 0;
    private static int _targetY = 0;
    private static bool _isPossibleAddCell = false;
    private static EnumResourceExtractionMode _targetCurrentMode = 
        EnumResourceExtractionMode.modeRemoveCell;

    private GameObject _resurceCell = null;
    private GameObject _currentCell = null;
    private Vector3 _createPosition;

    public static CellType[,] ArrayMap = null;
    public static CellType[,] CurrentArrayMap = null;
    public static CollectingItemsFromMap[,] identity = null;
    public static int cardWidth = 0;
    public static int cardHeight = 0;

    public CellType currentCardType;

    public enum ArrayMapOrCurrentArrayMap
    {
        ArrayMap,
        CurrentArrayMap
    }

    public enum EnumResourceExtractionMode
    {
        modeRemoveCell,
        modeInstallationCell
    }

    void Awake()
    {
        _createPosition = transform.position;

        if(_parentObject == null)
        {
            _parentObject = transform.parent.gameObject;
        }

        if (_levelParameters == null)
        {
            _levelParameters =
                Managers.GameParameters.DictionaryLevelParameters;

            _prefabMapArea =
                (CollectingItemsFromMap) _levelParameters["PrefabMapArea"];
            _prefabAirEmpty = 
                (GameObject) _levelParameters["PrefabAirEmpty"];
            _prefabAirEmptyAir = 
                (GameObject) _levelParameters["PrefabAirEmptyAir"];
            _prefabAirLandAir = 
                (GameObject) _levelParameters["PrefabAirLandAir"];
            _prefabAirLandEmpty = 
                (GameObject) _levelParameters["PrefabAirLandEmpty"];
            _prefabAirLandLand = 
                (GameObject) _levelParameters["PrefabAirLandLand"];
            _prefabEmpty = 
                (GameObject) _levelParameters["PrefabEmpty"];
            _prefabEmptyAir = 
                (GameObject) _levelParameters["PrefabEmptyAir"];
            _prefabEmptyLandAir = 
                (GameObject) _levelParameters["PrefabEmptyLandAir"];
            _prefabEmptyLandEmpty = 
                (GameObject) _levelParameters["PrefabEmptyLandEmpty"];
            _prefabEmptyLandLand = 
                (GameObject) _levelParameters["PrefabEmptyLandLand"];
            _prefabLand = 
                (GameObject) _levelParameters["PrefabLand"];
            _prefabLandLandAir = 
                (GameObject) _levelParameters["PrefabLandLandAir"];
            _prefabLandLandEmpty = 
                (GameObject) _levelParameters["PrefabLandLandEmpty"];
            _prefabResurce = 
                (GameObject) _levelParameters["PrefabResurce"];
            _prefabToHighlightAddition = 
                (GameObject) _levelParameters["PrefabToHighlightAddition"];
        }
    }
        
    public void CreateItemArea(int x, int y, 
        ArrayMapOrCurrentArrayMap arrayMapOrCurrentArrayMap = 
        ArrayMapOrCurrentArrayMap.ArrayMap)
    {
        CellType[,] itemArrayMap = null;

        if (arrayMapOrCurrentArrayMap == ArrayMapOrCurrentArrayMap.ArrayMap)
        {
            itemArrayMap = ArrayMap;
        }
        else if (
            arrayMapOrCurrentArrayMap ==
            ArrayMapOrCurrentArrayMap.CurrentArrayMap
        )
        {
            itemArrayMap = CurrentArrayMap;
        }

        if (itemArrayMap == null)
        {
            return;
        }

        cardWidth = itemArrayMap.GetUpperBound(0) + 1;
        cardHeight = itemArrayMap.GetUpperBound(1) + 1;

        if(
            (x < 0) || 
            (x >= cardWidth) || 
            (y < 0) || 
            (y >= cardHeight)
        )
        {
            return;
        }

        if(identity == null)
        {
            identity = new CollectingItemsFromMap[cardWidth, cardHeight];
        }

        identity[x, y] = this;

        currentCardType = itemArrayMap[x, y];

        if(
            (currentCardType == CellType.Air) || 
            (currentCardType == CellType.EmptyLot)
        )
        {
            BoxCollider _boxCollider = GetComponent<BoxCollider>();
            Destroy(_boxCollider);
        }

        if (x == 0)
        {
            if(
                (itemArrayMap[x, y] == CellType.Land) &&
                (itemArrayMap[x + 1, y] == CellType.Land)
            )
            {
                _currentCell = Instantiate(_prefabAirLandLand, _createPosition, 
                    Quaternion.identity, transform);
            }
            else if(
                (itemArrayMap[x, y] == CellType.Land) &&
                (
                    (itemArrayMap[x + 1, y] != CellType.Land) &&
                    (itemArrayMap[x + 1, y] != CellType.Air)
                )
            )
            {
                _currentCell = Instantiate(_prefabAirLandEmpty, _createPosition, 
                    Quaternion.identity, transform);
            }
            else if(
                (itemArrayMap[x, y] == CellType.Land) &&
                (itemArrayMap[x + 1, y] == CellType.Air)
            )
            {
                _currentCell = Instantiate(_prefabAirLandAir, _createPosition, 
                    Quaternion.identity, transform);
            }
            else if(
                (
                    (itemArrayMap[x, y] != CellType.Land) &&
                    (itemArrayMap[x, y] != CellType.Air)
                ) &&
                (itemArrayMap[x + 1, y] == CellType.Land)
            )
            {
                _currentCell = Instantiate(_prefabAirEmpty, _createPosition,
                    Quaternion.identity, transform);
            }
            else if(
                (
                    (itemArrayMap[x, y] != CellType.Land) &&
                    (itemArrayMap[x, y] != CellType.Air)
                ) &&
                (
                    (itemArrayMap[x + 1, y] != CellType.Land) &&
                    (itemArrayMap[x + 1, y] != CellType.Air)
                )
            )
            {
                _currentCell = Instantiate(_prefabAirEmpty, _createPosition,
                    Quaternion.identity, transform);
            }
            else if(
                (
                    (itemArrayMap[x, y] != CellType.Land) &&
                    (itemArrayMap[x, y] != CellType.Air)
                ) &&
                (itemArrayMap[x + 1, y] == CellType.Air)
            )
            {
                _currentCell = Instantiate(_prefabAirEmptyAir, _createPosition,
                    Quaternion.identity, transform);
            }
        }
        else if(x == cardWidth - 1)
        {
            if (
                (itemArrayMap[x - 1, y] == CellType.Land) &&
                (itemArrayMap[x, y] == CellType.Land)
            )
            {
                _currentCell = Instantiate(_prefabLandLandAir, _createPosition,
                    Quaternion.identity, transform);
            }
            else if (
                (
                    (itemArrayMap[x - 1, y] != CellType.Land) &&
                    (itemArrayMap[x - 1, y] != CellType.Air)
                ) &&
                (itemArrayMap[x, y] == CellType.Land)
            )
            {
                _currentCell = Instantiate(_prefabEmptyLandAir, _createPosition,
                    Quaternion.identity, transform);
            }
            else if (
                (itemArrayMap[x - 1, y] == CellType.Air) &&
                (itemArrayMap[x, y] == CellType.Land)
            )
            {
                _currentCell = Instantiate(_prefabAirLandAir, _createPosition,
                    Quaternion.identity, transform);
            }
            else if (
                (itemArrayMap[x - 1, y] == CellType.Land) &&
                (
                    (itemArrayMap[x, y] != CellType.Land) &&
                    (itemArrayMap[x, y] != CellType.Air)
                )
            )
            {
                _currentCell = Instantiate(_prefabEmptyAir, _createPosition,
                    Quaternion.identity, transform);
            }
            else if (
                (
                    (itemArrayMap[x - 1, y] != CellType.Land) &&
                    (itemArrayMap[x - 1, y] != CellType.Air)
                ) &&
                (
                    (itemArrayMap[x, y] != CellType.Land) &&
                    (itemArrayMap[x, y] != CellType.Air)
                )
            )
            {
                _currentCell = Instantiate(_prefabEmptyAir, _createPosition,
                    Quaternion.identity, transform);
            }
            else if (
                (itemArrayMap[x - 1, y] == CellType.Air) &&
                (
                    (itemArrayMap[x, y] != CellType.Land) &&
                    (itemArrayMap[x, y] != CellType.Air)
                )
            )
            {
                _currentCell = Instantiate(_prefabAirEmptyAir, _createPosition,
                    Quaternion.identity, transform);
            }
        }
        else if(
            (itemArrayMap[x - 1, y] == CellType.Air) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (itemArrayMap[x + 1, y] == CellType.Land)
        )
        {
            _currentCell = Instantiate(_prefabAirLandLand, _createPosition, 
                Quaternion.identity, transform);
        }
        else if(
            (itemArrayMap[x - 1, y] == CellType.Air) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (itemArrayMap[x + 1, y] == CellType.Air)
        )
        {
            _currentCell = Instantiate(_prefabAirLandAir, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Land) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (itemArrayMap[x + 1, y] == CellType.Air)
        )
        {
            _currentCell = Instantiate(_prefabLandLandAir, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Air) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (
                (itemArrayMap[x + 1, y] != CellType.Land) &&
                (itemArrayMap[x + 1, y] != CellType.Air)
            )
        )
        {
            _currentCell = Instantiate(_prefabAirLandEmpty, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (
                (itemArrayMap[x - 1, y] != CellType.Land) &&
                (itemArrayMap[x - 1, y] != CellType.Air)
            ) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (itemArrayMap[x + 1, y] == CellType.Air)
        )
        {
            _currentCell = Instantiate(_prefabEmptyLandAir, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (
                (itemArrayMap[x - 1, y] != CellType.Land) &&
                (itemArrayMap[x - 1, y] != CellType.Air)
            ) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (
                (itemArrayMap[x + 1, y] != CellType.Land) &&
                (itemArrayMap[x + 1, y] != CellType.Air)
            )
        )
        {
            _currentCell = Instantiate(_prefabEmptyLandEmpty, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Land) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (itemArrayMap[x + 1, y] == CellType.Land)
        )
        {
            _currentCell = Instantiate(_prefabLand, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Land) &&
            (
                (itemArrayMap[x, y] != CellType.Land) &&
                (itemArrayMap[x, y] != CellType.Air)
            ) &&
            (
                (
                    (itemArrayMap[x + 1, y] != CellType.Land) &&
                    (itemArrayMap[x + 1, y] != CellType.Air)
                ) ||
                (itemArrayMap[x + 1, y] == CellType.Land)
            )
        )
        {
            _currentCell = Instantiate(_prefabEmpty, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (
                (itemArrayMap[x - 1, y] != CellType.Land) &&
                (itemArrayMap[x - 1, y] != CellType.Air)
            ) &&
            (
                (itemArrayMap[x, y] != CellType.Land) &&
                (itemArrayMap[x, y] != CellType.Air)
            ) &&
            (
                (
                    (itemArrayMap[x + 1, y] != CellType.Land) &&
                    (itemArrayMap[x + 1, y] != CellType.Air)
                ) ||
                (itemArrayMap[x + 1, y] == CellType.Land)
            )
        )
        {
            _currentCell = Instantiate(_prefabEmpty, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Air) &&
            (
                (itemArrayMap[x, y] != CellType.Land) &&
                (itemArrayMap[x, y] != CellType.Air)
            ) &&
            (itemArrayMap[x + 1, y] == CellType.Air)
        )
        {
            _currentCell = Instantiate(_prefabAirEmptyAir, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Air) &&
            (
                (itemArrayMap[x, y] != CellType.Land) &&
                (itemArrayMap[x, y] != CellType.Air)
            ) &&
            (
                (
                    (itemArrayMap[x + 1, y] != CellType.Land) &&
                    (itemArrayMap[x + 1, y] != CellType.Air)
                ) ||
                (itemArrayMap[x + 1, y] == CellType.Land)
            )
        )
        {
            _currentCell = Instantiate(_prefabAirEmpty, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (
                (itemArrayMap[x - 1, y] != CellType.Land) &&
                (itemArrayMap[x - 1, y] != CellType.Air)
            ) &&
            (
                (itemArrayMap[x, y] != CellType.Land) &&
                (itemArrayMap[x, y] != CellType.Air)
            ) &&
            (itemArrayMap[x + 1, y] == CellType.Air)
        )
        {
            _currentCell = Instantiate(_prefabEmptyAir, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Land) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (
                (itemArrayMap[x + 1, y] != CellType.Land) &&
                (itemArrayMap[x + 1, y] != CellType.Air)
            )
        )
        {
            _currentCell = Instantiate(_prefabLandLandEmpty, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (
                (itemArrayMap[x - 1, y] != CellType.Land) &&
                (itemArrayMap[x - 1, y] != CellType.Air)
            ) &&
            (itemArrayMap[x, y] == CellType.Land) &&
            (itemArrayMap[x + 1, y] == CellType.Land)
        )
        {
            _currentCell = Instantiate(_prefabEmptyLandLand, _createPosition, 
                Quaternion.identity, transform);
        }
        else if (
            (itemArrayMap[x - 1, y] == CellType.Land) &&
            (
                (itemArrayMap[x, y] != CellType.Land) &&
                (itemArrayMap[x, y] != CellType.Air)
            ) &&
            (itemArrayMap[x + 1, y] == CellType.Air)
        )
        {
            _currentCell = Instantiate(_prefabEmptyAir, _createPosition, 
                Quaternion.identity, transform);
        }

        if (
            (y > 0) && 
            (y < cardHeight - 1) && 
            (x > 0) && 
            (x < cardWidth - 1)
        )
        {
            if(
                (itemArrayMap[x, y + 1] == CellType.Land) &&
                (itemArrayMap[x - 1, y + 1] == CellType.Land) &&
                (itemArrayMap[x + 1, y + 1] == CellType.Land) &&
                (itemArrayMap[x, y - 1] == CellType.Land) &&
                (itemArrayMap[x - 1, y - 1] == CellType.Land) &&
                (itemArrayMap[x + 1, y - 1] == CellType.Land)
            )
            {
                _currentCell.SendMessage("DeleteSurfaceElements", 
                    SendMessageOptions.DontRequireReceiver);
            }
        }
        
        Color currentColor = Color.white;
        Color colorIron = (Color) _levelParameters["ColorIron"];
        Color colorUranus = (Color) _levelParameters["ColorUranus"];

        if (itemArrayMap[x, y] == CellType.Iron)
        {
            currentColor = colorIron;
        }
        else if (itemArrayMap[x, y] == CellType.Uranus)
        {
            currentColor = colorUranus;
        }

        if (
            (itemArrayMap[x, y] != CellType.EmptyLot) &&
            (itemArrayMap[x, y] != CellType.Air) &&
            (itemArrayMap[x, y] != CellType.Land)
        )
        {
            _resurceCell = Instantiate(_prefabResurce, new Vector3(x, y, 0), 
                Quaternion.identity, transform);

            _resurceCell.SendMessage("SetColorDefault", currentColor, 
                SendMessageOptions.DontRequireReceiver);
        }
    }

    public static void SetTargetXY(int x, int y, 
        EnumResourceExtractionMode currentMode)
    {
        if(
            (identity == null) || 
            (x > cardWidth - 1) || 
            (x < 0) || 
            (y > cardHeight - 1) || 
            (y < 0)
        )
        {
            return;
        }

        if(
            (x != _targetX) || 
            (y != _targetY) || 
            (_targetCurrentMode != currentMode)
        )
        {
            _targetCurrentMode = currentMode;

            if(_targetCurrentMode == EnumResourceExtractionMode.modeRemoveCell)
            {
                DestriyElementToHighlightAddition();
            }

            if (identity[_targetX, _targetY] != null)
            {
                identity[_targetX, _targetY].DeselectCellToRemove();
            }
            
            _targetX = x;
            _targetY = y;

            if (
                (
                    _targetCurrentMode == 
                    EnumResourceExtractionMode.modeRemoveCell
                ) && 
                (identity[_targetX, _targetY] != null)
            )
            {
                identity[_targetX, _targetY].SelectCellToRemove();
            }
            else if(
                _targetCurrentMode == 
                EnumResourceExtractionMode.modeInstallationCell
            )
            {
                SelectCellToAdd();
            }
        }
    }

    public static void Operate()
    {
        if (
            (identity == null) ||
            (_targetX > cardWidth - 1) ||
            (_targetX < 0) ||
            (_targetY > cardHeight - 1) ||
            (_targetY <= 0)
        )
        {
            return;
        }

        if(_targetCurrentMode == EnumResourceExtractionMode.modeRemoveCell)
        {
            if (
                (CurrentArrayMap[_targetX, _targetY] == CellType.Land) || 
                (
                    (
                        CurrentArrayMap[_targetX, _targetY] != CellType.Land
                    ) &&
                    (
                        CurrentArrayMap[_targetX, _targetY] != CellType.EmptyLot
                    ) &&
                    (CurrentArrayMap[_targetX, _targetY] != CellType.Air)
                )
            )
            {
                switch(CurrentArrayMap[_targetX, _targetY])
                {
                    case CellType.Land:
                        Managers.Inventory
                            .AddItem(ItemsMayInventory.Stones, 1000);
                        break;
                    case CellType.Iron:
                        Managers.Inventory
                            .AddItem(ItemsMayInventory.Iron, 1000);
                        break;
                    case CellType.Uranus:
                        Managers.Inventory
                            .AddItem(ItemsMayInventory.Uranus, 1000);
                        break;
                }

                if(ArrayMap[_targetX, _targetY] == CellType.Air)
                {
                    CurrentArrayMap[_targetX, _targetY] = CellType.Air;
                }
                else
                {
                    CurrentArrayMap[_targetX, _targetY] = CellType.EmptyLot;
                }
            }
        }
        else if(
            _targetCurrentMode == 
            EnumResourceExtractionMode.modeInstallationCell
        )
        {
            if(
                _isPossibleAddCell &&
                (
                    (CurrentArrayMap[_targetX, _targetY] == CellType.Air) ||
                    (CurrentArrayMap[_targetX, _targetY] == CellType.EmptyLot)
                )
            )
            {
                StatusesUsingItemsInventory statusesUsing = Managers.Inventory
                    .ConsumeItem(ItemsMayInventory.Stones, 1000);
                
                if(statusesUsing == StatusesUsingItemsInventory.Used)
                {
                    CurrentArrayMap[_targetX, _targetY] = CellType.Land;
                }
            }
        }

        RecreateCell(_targetX, _targetY);
        if (_targetX > 0)
        {
            RecreateCell(_targetX - 1, _targetY);
        }
        if ((_targetX > 0) && (_targetY < cardHeight - 1))
        {
            RecreateCell(_targetX - 1, _targetY + 1);
        }
        if (_targetY < cardHeight - 1)
        {
            RecreateCell(_targetX, _targetY + 1);
        }
        if (
            (_targetX < cardWidth - 1) &&
            (_targetY < cardHeight - 1)
        )
        {
            RecreateCell(_targetX + 1, _targetY + 1);
        }
        if (_targetX < cardWidth - 1)
        {
            RecreateCell(_targetX + 1, _targetY);
        }
        if (
            (_targetX < cardWidth - 1) &&
            (_targetY > 0)
        )
        {
            RecreateCell(_targetX + 1, _targetY - 1);
        }
        if (_targetY > 0)
        {
            RecreateCell(_targetX, _targetY - 1);
        }
        if (
            (_targetX > 0) &&
            (_targetY > 0)
        )
        {
            RecreateCell(_targetX - 1, _targetY - 1);
        }
    }

    private static void RecreateCell(int x, int y)
    {
        if(identity[x, y] != null)
        {
            Destroy(identity[x, y].gameObject);
        }

        if(CurrentArrayMap[x, y] == CellType.Air)
        {
            return;
        }

        CollectingItemsFromMap itemMapElements = Instantiate(_prefabMapArea, 
            new Vector3(x, y, 0), Quaternion.identity, _parentObject.transform);

        itemMapElements.CreateItemArea(x, y, 
            ArrayMapOrCurrentArrayMap.CurrentArrayMap);
    }

    private void SelectCellToRemove()
    {
        if(_currentCell != null)
        {
            _currentCell.SendMessage("SelectCellToRemove", 
                SendMessageOptions.DontRequireReceiver);
        }
        if(_resurceCell != null)
        {
            _resurceCell.SendMessage("SelectCellToRemove",
                SendMessageOptions.DontRequireReceiver);
        }
    }

    private void DeselectCellToRemove()
    {
        if (_currentCell != null)
        {
            _currentCell.SendMessage("DeselectCellToRemove",
                SendMessageOptions.DontRequireReceiver);
        }
        if (_resurceCell != null)
        {
            _resurceCell.SendMessage("DeselectCellToRemove",
                SendMessageOptions.DontRequireReceiver);
        }
    }

    private static void SelectCellToAdd()
    {
        if(
            (_targetX == 0) || 
            (_targetX == cardWidth - 1) || 
            (_targetY == 0) || 
            (_targetY == cardHeight - 1)
        )
        {
            DestriyElementToHighlightAddition();
            return;
        }

        if (
            (
                (CurrentArrayMap[_targetX, _targetY] != CellType.EmptyLot) &&
                (CurrentArrayMap[_targetX, _targetY] != CellType.Air)
            ) ||
            (
                ConstructionOnMap
                .ArrayConstructionObjectsOnMap[_targetX, _targetY] != 
                ConstructionOnMap.TypesConstructionObjects.Empty
            )
        )
        {
            DestriyElementToHighlightAddition();
            return;
        }

        if(
            (CurrentArrayMap[_targetX - 1, _targetY] == CellType.Air) &&
            (CurrentArrayMap[_targetX, _targetY + 1] == CellType.Air) &&
            (CurrentArrayMap[_targetX + 1, _targetY] == CellType.Air) &&
            (CurrentArrayMap[_targetX, _targetY - 1] == CellType.Air) &&
            (
                ConstructionOnMap
                .ArrayConstructionObjectsOnMap[_targetX - 1, _targetY] ==
                ConstructionOnMap.TypesConstructionObjects.Empty
            ) &&
            (
                ConstructionOnMap
                .ArrayConstructionObjectsOnMap[_targetX, _targetY + 1] ==
                ConstructionOnMap.TypesConstructionObjects.Empty
            ) &&
            (
                ConstructionOnMap
                .ArrayConstructionObjectsOnMap[_targetX + 1, _targetY] ==
                ConstructionOnMap.TypesConstructionObjects.Empty
            ) &&
            (
                ConstructionOnMap
                .ArrayConstructionObjectsOnMap[_targetX, _targetY - 1] ==
                ConstructionOnMap.TypesConstructionObjects.Empty
            )
        )
        {
            DestriyElementToHighlightAddition();
            return;
        }

        CreateElementToHighlightAddition();

        _elementToHighlightAddition.transform.position = 
            new Vector3(_targetX, _targetY, 0);
    }

    private static void CreateElementToHighlightAddition()
    {
        _isPossibleAddCell = true;

        if (_elementToHighlightAddition != null)
        {
            return;
        }

        _elementToHighlightAddition = Instantiate(_prefabToHighlightAddition, 
            Vector3.zero, Quaternion.identity, 
            _parentObject.transform);
    }

    private static void DestriyElementToHighlightAddition()
    {
        _isPossibleAddCell = false;

        if (_elementToHighlightAddition == null)
        {
            return;
        }

        Destroy(_elementToHighlightAddition);
    }
}
