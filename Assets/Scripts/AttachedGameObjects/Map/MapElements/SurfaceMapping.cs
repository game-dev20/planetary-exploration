﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceMapping : MonoBehaviour
{
    [SerializeField] private GameObject[] _elementsToDelete;

    public void DeleteSurfaceElements()
    {
        foreach(GameObject itemElement in _elementsToDelete)
        {
            Destroy(itemElement);
        }
    }
}
