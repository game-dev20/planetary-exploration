﻿using System.Collections;
using UnityEngine;

public class SelectingCell : MonoBehaviour
{
    [SerializeField] private Renderer[] _elementsToHighlight;

    private Color _colorDefault = Color.white;
    private static Color _colorActive = Color.white;

    void Awake()
    {
        if(_colorActive == Color.white)
        {
            _colorActive = Managers.GameParameters.DataMainParameters
                .ColorSelectedCellDelete;
        }
    }

    public void SetColorDefault(Color color)
    {
        _colorDefault = color;

        SetColorToElements(color);
    }

    private void SetColorToElements(Color color)
    {
        foreach (Renderer itemElement in _elementsToHighlight)
        {
            if (itemElement == null)
            {
                continue;
            }

            itemElement.material.color = color;
        }
    }

    public void SelectCellToRemove()
    {
        SetColorToElements(_colorActive);
    }

    public void DeselectCellToRemove()
    {
        SetColorToElements(_colorDefault);
    }
}
