﻿using System;
using System.Reflection;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerGameParameters : MonoBehaviour, IGameManager
{
    [SerializeField] private LevelParameters _levelParameters;
    [SerializeField] private MainParameters _mainParameters;

    public ManagerStatus status { get; private set; }

    public LevelParameters DataLevelParameters { get; private set; }
    public Dictionary<string, System.Object> DictionaryLevelParameters
    {
        get; 
        private set; 
    }

    public MainParameters DataMainParameters { get; private set; }
    public Dictionary<string, System.Object> DictionaryMainParameters
    {
        get;
        private set;
    }

    public void Startup()
    {
        Debug.Log("GameParameters manager started...");

        DataLevelParameters = _levelParameters;
        DataMainParameters = _mainParameters;

        DictionaryLevelParameters = GetParametersAsDictionary(_levelParameters);
        DictionaryMainParameters = GetParametersAsDictionary(_mainParameters);

        status = ManagerStatus.Started;
    }

    private Dictionary<string, System.Object> GetParametersAsDictionary(
        ScriptableObject parametersSection)
    {
        Dictionary<string, System.Object> parameters = new Dictionary<string,
            System.Object>();

        Type typeParameters = parametersSection.GetType();

        FieldInfo[] fieldParameters = typeParameters.GetFields();

        foreach(FieldInfo itemParameter in fieldParameters)
        {
            string itemNameParameter = itemParameter.Name;
            System.Object itemValueParameter = itemParameter.GetValue(
                parametersSection);
            parameters.Add(itemNameParameter, itemValueParameter);
        }

        return parameters;
    }
}
