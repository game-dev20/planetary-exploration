﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerLoadLevel : MonoBehaviour, IGameManager
{
    [SerializeField] private GeneratorPlanetMapArray _genetatorMapArray;
    [SerializeField] private BuilderGameObjectsFromMapArray _builderMap;
    [SerializeField] private BuildingGameObjectsTreesFromMapArray _builderTrees;
    [SerializeField] private CreatingPlayerOnMap _creatingPlayer;

    public ManagerStatus status { get; private set; }

    private string _nameLoadScene;
    private GameObject _generatedMap;

    public void Startup()
    {
        Debug.Log("LoadLevel manager started...");

        _nameLoadScene = Managers
            .GameParameters
            .DataMainParameters
            .PlanetSceneName;

        Messenger.AddListener(
            LoadEvents.COMPLETION_LOADING_MODULES, 
            StartLoadLevel
        );
        
        status = ManagerStatus.Started;
    }

    private void StartLoadLevel()
    {
        Debug.Log("Start building a level...");

        StartCoroutine(RunLoadLevel());
    }

    private IEnumerator RunLoadLevel()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(
            _nameLoadScene, 
            LoadSceneMode.Additive
        );

        while (!asyncLoad.isDone)
        {
            Debug.Log("Loading Chene Proccess... " + asyncLoad.progress * 100);
            yield return null;
        }
        Debug.Log("Loading Chene Proccess... " + asyncLoad.progress * 100);

        Debug.Log("Start generating the planet map array...");
        _genetatorMapArray.Generate();
        Debug.Log("Finish generating the planet map array...");

        yield return null;

        Debug.Log("Start building game objects on the stage...");
        CellType[,] arrayMap = _genetatorMapArray.LevelMap;
        _generatedMap = _builderMap.BuildingMap(arrayMap);
        Debug.Log("Finish building game objects on the stage...");

        yield return null;

        Debug.Log("Start building game objects of trees right on stage...");
        _builderTrees.InitialData(arrayMap, _generatedMap);
        _builderTrees.BuildingMap();
        Debug.Log("Finish building game objects of trees right on stage...");

        _creatingPlayer.InitialData(arrayMap, _generatedMap);
        GameObject player = _creatingPlayer.CreatePlayerOnMap();

        Scene currentScene = SceneManager.GetActiveScene();
        Scene levelScene = SceneManager.GetSceneByName(_nameLoadScene);
        SceneManager.MoveGameObjectToScene(_generatedMap, levelScene);

        SceneManager.UnloadSceneAsync(currentScene);

        yield return null;

        Messenger<Transform>.Broadcast(LoadEvents.PLANET_LEVEL_BUILT, 
            player.transform);

        Debug.Log("Level built.");
    }
}
