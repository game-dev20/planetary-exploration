﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerInvertory : MonoBehaviour, IGameManager
{
    public ManagerStatus status { get; private set; }
    
    private Dictionary<string, int> _items;

    public void Startup()
    {
        Debug.Log("Invertory manager starting...");

        UpdateData(new Dictionary<string, int>());

        //------------
        AddItem(ItemsMayInventory.FuelCell, 9999);
        AddItem(ItemsMayInventory.RecycledIron, 9999);
        //------------

        status = ManagerStatus.Started;
    }

    public void UpdateData(Dictionary<string, int> items)
    {
        _items = items;
    }

    public Dictionary<string, int> GetData()
    {
        return _items;
    }

    private void DisplayItems()
    {
        string itemDisplay = "Items: ";

        foreach (KeyValuePair<string, int> item in _items)
        {
            itemDisplay += item.Key + "(" + item.Value + ") ";
        }

        Debug.Log(itemDisplay);
    }

    public void AddItem(string name, int count)
    {
        if (_items.ContainsKey(name))
        {
            _items[name] += count;
        }
        else
        {
            _items[name] = count;
        }

        DisplayItems();
    }

    public List<string> GetItemList()
    {
        List<string> list = new List<string>(_items.Keys);

        return list;
    }

    public int GetItemCount(string name)
    {
        if (_items.ContainsKey(name))
        {
            return _items[name];
        }

        return 0;
    }

    public StatusesUsingItemsInventory ConsumeItem(string name, int count)
    {
        if (_items.ContainsKey(name))
        {
            if(_items[name] >= count)
            {
                _items[name] -= count;
                if (_items[name] == 0)
                {
                    _items.Remove(name);
                }
            }
            else
            {
                Debug.Log("Not enough in inventory " + name);
                return StatusesUsingItemsInventory.NotEnoughUse;
            }
        }
        else
        {
            Debug.Log("Not in inventory " + name);
            return StatusesUsingItemsInventory.NotInInventory;
        }

        DisplayItems();
        return StatusesUsingItemsInventory.Used;
    }
}
