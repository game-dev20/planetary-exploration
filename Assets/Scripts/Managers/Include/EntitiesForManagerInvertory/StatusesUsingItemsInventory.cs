﻿public enum StatusesUsingItemsInventory
{
    Used,
    NotInInventory,
    NotEnoughUse
}
