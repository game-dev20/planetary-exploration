﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SimpleLocalization;

public class ManagerLoacalization : MonoBehaviour, IGameManager
{
    public ManagerStatus status { get; private set; }

    public string DefaultLanguage = "English";

    public void Startup()
    {
        Debug.Log("Loacalization manager starting...");

        InitialLocalization();

        status = ManagerStatus.Started;
    }

    public void InitialLocalization()
    {
        LocalizationManager.Read();

        switch (Application.systemLanguage)
        {
            case SystemLanguage.Russian:
                LocalizationManager.Language = "Russian";
                break;
            default:
                LocalizationManager.Language = DefaultLanguage;
                break;
        }
    }
}
