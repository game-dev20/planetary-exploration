﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(ManagerGameParameters))]
[RequireComponent(typeof(ManagerLoadLevel))]
[RequireComponent(typeof(ManagerInvertory))]
[RequireComponent(typeof(ManagerLoacalization))]

public class Managers : MonoBehaviour
{
    public static ManagerGameParameters GameParameters { get; private set; }
    public static ManagerLoadLevel LoadLevel { get; private set; }
    public static ManagerInvertory Inventory { get; private set; }
    public static ManagerLoacalization Loacalization { get; private set; }

    private List<IGameManager> _startSequence;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        
        GameParameters = GetComponent<ManagerGameParameters>();
        LoadLevel = GetComponent<ManagerLoadLevel>();
        Inventory = GetComponent<ManagerInvertory>();
        Loacalization = GetComponent<ManagerLoacalization>();

        _startSequence = new List<IGameManager>();
        _startSequence.Add(GameParameters);
        _startSequence.Add(LoadLevel);
        _startSequence.Add(Inventory);
        _startSequence.Add(Loacalization);

        StartCoroutine(StartupManagers());
    }

    private IEnumerator StartupManagers()
    {
        foreach(IGameManager manager in _startSequence)
        {
            manager.Startup();
        }

        yield return null;

        int numModules = _startSequence.Count;
        int numReady = 0;

        while(numReady < numModules)
        {
            int lastReady = numReady;
            numReady = 0;

            foreach(IGameManager manager in _startSequence)
            {
                if(manager.status == ManagerStatus.Started)
                {
                    numReady++;
                }
            }

            if (numReady > lastReady)
            {
                Debug.Log("Progress: " + numReady + "/" + numModules);
            }

            yield return null;
        }

        Debug.Log("All managers started up");
        Messenger.Broadcast(LoadEvents.COMPLETION_LOADING_MODULES);
    }
}
