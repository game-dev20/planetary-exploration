﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{
    [SerializeField] private Text _label;

    public float updateInterval = 0.5F;
    
    private double lastInterval;
    private int frames = 0;
    private float fps;
    
    void Start()
    {
        lastInterval = Time.realtimeSinceStartup;
        frames = 0;
    }

    void Update()
    {
        ++frames;
        float timeNow = Time.realtimeSinceStartup;
        if (timeNow > lastInterval + updateInterval)
        {
            fps = (float)(frames / (timeNow - lastInterval));
            frames = 0;
            lastInterval = timeNow;

            _label.text = "FPS " + fps.ToString("f2");
        }
    }
}