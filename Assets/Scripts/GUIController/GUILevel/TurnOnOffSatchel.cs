﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnOnOffSatchel : MonoBehaviour
{
    [SerializeField] private Button _turnOnSatchel;
    [SerializeField] private Button _turnOffSatchel;

    void Awake()
    {
        Messenger.AddListener(InventoryEvents.FUEL_BACKPACK_OUT,
            SettingKnapsackButtonDefaultState);

        SettingKnapsackButtonDefaultState();
    }

    void Destroy()
    {
        Messenger.RemoveListener(InventoryEvents.FUEL_BACKPACK_OUT,
            SettingKnapsackButtonDefaultState);
    }

    void SettingKnapsackButtonDefaultState()
    {
        _turnOnSatchel.gameObject.SetActive(true);
        _turnOffSatchel.gameObject.SetActive(false);
    }

    public void TurnOnSatchel()
    {
        _turnOnSatchel.gameObject.SetActive(false);
        _turnOffSatchel.gameObject.SetActive(true);
        Messenger<bool>.Broadcast(GUIEvents.TURN_ON_OR_OFF_SATCHEL, true);
    }

    public void TurnOffSatchel()
    {
        SettingKnapsackButtonDefaultState();
        Messenger<bool>.Broadcast(GUIEvents.TURN_ON_OR_OFF_SATCHEL, false);
    }
}
