﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceExtractionMode : MonoBehaviour
{
    [SerializeField] private Button _onResourceExtractionMode;
    [SerializeField] private Button _offResourceExtractionMode;
    [SerializeField] private Button _modeRemoveCell;
    [SerializeField] private Button _modeInstallationCell;

    void SettingButtonDefaultState()
    {
        _onResourceExtractionMode.gameObject.SetActive(true);
        _offResourceExtractionMode.gameObject.SetActive(false);
        _modeRemoveCell.gameObject.SetActive(false);
        _modeInstallationCell.gameObject.SetActive(false);
    }

    void Awake()
    {
        Messenger.AddListener(GUIEvents.OFF_RESOURCE_EXTRACTION_MODE, 
            OffResourceExtractionMode);

        SettingButtonDefaultState();
    }

    void Destroy()
    {
        Messenger.RemoveListener(GUIEvents.OFF_RESOURCE_EXTRACTION_MODE, 
            OffResourceExtractionMode);
    }

    public void OnResourceExtractionMode()
    {
        _onResourceExtractionMode.gameObject.SetActive(false);
        _offResourceExtractionMode.gameObject.SetActive(true);
        _modeRemoveCell.gameObject.SetActive(false);
        _modeInstallationCell.gameObject.SetActive(true);

        Messenger.Broadcast(GUIEvents.OFF_CONSTRUCTION_MODE);

        Messenger<bool, CollectingItemsFromMap.EnumResourceExtractionMode>
            .Broadcast(GUIEvents.ON_OR_OFF_RESOURCE_EXTRACTION_MODE, true, 
            CollectingItemsFromMap.EnumResourceExtractionMode.modeRemoveCell);
    }

    public void OffResourceExtractionMode()
    {
        SettingButtonDefaultState();

        Messenger<bool, CollectingItemsFromMap.EnumResourceExtractionMode>
            .Broadcast(GUIEvents.ON_OR_OFF_RESOURCE_EXTRACTION_MODE, false, 
            CollectingItemsFromMap.EnumResourceExtractionMode.modeRemoveCell);
    }

    public void ModeRemoveCell()
    {
        _modeRemoveCell.gameObject.SetActive(false);
        _modeInstallationCell.gameObject.SetActive(true);

        Messenger<bool, CollectingItemsFromMap.EnumResourceExtractionMode>
            .Broadcast(GUIEvents.ON_OR_OFF_RESOURCE_EXTRACTION_MODE, true,
            CollectingItemsFromMap.EnumResourceExtractionMode
            .modeRemoveCell);
    }

    public void ModeInstallationCell()
    {
        _modeRemoveCell.gameObject.SetActive(true);
        _modeInstallationCell.gameObject.SetActive(false);

        Messenger<bool, CollectingItemsFromMap.EnumResourceExtractionMode>
            .Broadcast(GUIEvents.ON_OR_OFF_RESOURCE_EXTRACTION_MODE, true,
            CollectingItemsFromMap.EnumResourceExtractionMode
            .modeInstallationCell);
    }
}
