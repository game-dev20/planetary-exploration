﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ConstructionMode : MonoBehaviour
{
    [SerializeField] private Button _onConstructionMode;
    [SerializeField] private Button _offConstructionMode;
    [SerializeField] private Button _choiceBlocks;
    [SerializeField] private Button _deletingBlocks;
    [SerializeField] private Button _buildingBlock;
    [SerializeField] private Button _blockDoor;

    private Color _defaultColor = new Color(1f, 1f, 1f, 0.47f);
    private Color _activeColor = new Color(1f, 0f, 0f, 0.47f);
    private bool _isClickChoiceBlocks = false;

    private enum ButtonStates
    {
        Hide,
        BlocksDelete,
        BlocksAdd
    }

    private void SetActiveButton(Button activeButton)
    {
        if(activeButton == _deletingBlocks)
        {
            _choiceBlocks.interactable = true;
        }
        else
        {
            _choiceBlocks.interactable = false;
        }
        
        ColorBlock colorBlock;

        colorBlock = _deletingBlocks.colors;
        colorBlock.normalColor = _defaultColor;
        colorBlock.highlightedColor = _defaultColor;
        colorBlock.selectedColor = _defaultColor;
        _deletingBlocks.colors = colorBlock;

        colorBlock = _buildingBlock.colors;
        colorBlock.normalColor = _defaultColor;
        colorBlock.highlightedColor = _defaultColor;
        colorBlock.selectedColor = _defaultColor;
        _buildingBlock.colors = colorBlock;

        colorBlock = _blockDoor.colors;
        colorBlock.normalColor = _defaultColor;
        colorBlock.highlightedColor = _defaultColor;
        colorBlock.selectedColor = _defaultColor;
        _blockDoor.colors = colorBlock;

        colorBlock = activeButton.colors;
        colorBlock.normalColor = _activeColor;
        colorBlock.highlightedColor = _activeColor;
        colorBlock.selectedColor = _activeColor;
        activeButton.colors = colorBlock;
    }

    private void ShowingSpecificButtons(ButtonStates states)
    {
        switch(states)
        {
            case ButtonStates.Hide:
                _onConstructionMode.gameObject.SetActive(true);
                _offConstructionMode.gameObject.SetActive(false);
                _choiceBlocks.gameObject.SetActive(false);
                _deletingBlocks.gameObject.SetActive(false);
                _buildingBlock.gameObject.SetActive(false);
                _blockDoor.gameObject.SetActive(false);
                _isClickChoiceBlocks = false;
                break;
            case ButtonStates.BlocksDelete:
                _onConstructionMode.gameObject.SetActive(false);
                _offConstructionMode.gameObject.SetActive(true);
                _choiceBlocks.gameObject.SetActive(true);
                _deletingBlocks.gameObject.SetActive(true);
                _buildingBlock.gameObject.SetActive(false);
                _blockDoor.gameObject.SetActive(false);
                _isClickChoiceBlocks = false;
                break;
            case ButtonStates.BlocksAdd:
                _onConstructionMode.gameObject.SetActive(false);
                _offConstructionMode.gameObject.SetActive(true);
                _choiceBlocks.gameObject.SetActive(true);
                _deletingBlocks.gameObject.SetActive(true);
                _buildingBlock.gameObject.SetActive(true);
                _blockDoor.gameObject.SetActive(true);
                _isClickChoiceBlocks = true;
                break;
        }
    }

    void Awake()
    {
        Messenger.AddListener(GUIEvents.OFF_CONSTRUCTION_MODE, 
            OffConstructionMode);

        ShowingSpecificButtons(ButtonStates.Hide);
    }

    void Destroy()
    {
        Messenger.RemoveListener(GUIEvents.OFF_CONSTRUCTION_MODE, 
            OffConstructionMode);
    }

    public void OnConstructionMode()
    {
        Messenger.Broadcast(GUIEvents.OFF_RESOURCE_EXTRACTION_MODE);

        DeletingBlocks();
    }

    public void OffConstructionMode()
    {
        ShowingSpecificButtons(ButtonStates.Hide);

        Messenger<bool, ConstructionOnMap.EnumModeConstruction>
            .Broadcast(GUIEvents.ON_OR_OFF_CONSTRUCTION_MODE, false,
            ConstructionOnMap.EnumModeConstruction.ModeRemove);
    }

    public void ChoiceBlocks()
    {
        if(_isClickChoiceBlocks)
        {
            ShowingSpecificButtons(ButtonStates.BlocksDelete);
        }
        else
        {
            ShowingSpecificButtons(ButtonStates.BlocksAdd);
        }
    }

    public void DeletingBlocks()
    {
        ShowingSpecificButtons(ButtonStates.BlocksDelete);

        SetActiveButton(_deletingBlocks);

        Messenger<bool, ConstructionOnMap.EnumModeConstruction>
            .Broadcast(GUIEvents.ON_OR_OFF_CONSTRUCTION_MODE, true, 
            ConstructionOnMap.EnumModeConstruction.ModeRemove);
    }

    public void BuildingBlock()
    {
        SetActiveButton(_buildingBlock);

        Messenger<bool, ConstructionOnMap.EnumModeConstruction>
            .Broadcast(GUIEvents.ON_OR_OFF_CONSTRUCTION_MODE, true,
            ConstructionOnMap.EnumModeConstruction.ModeAddWall);
    }

    public void BlockDoor()
    {
        SetActiveButton(_blockDoor);

        Messenger<bool, ConstructionOnMap.EnumModeConstruction>
            .Broadcast(GUIEvents.ON_OR_OFF_CONSTRUCTION_MODE, true,
            ConstructionOnMap.EnumModeConstruction.ModeAddDoor);
    }
}